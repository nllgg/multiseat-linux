# Verslag van multiseat computer experiment.

Tijdens de [NLLGG bijeenkomst op 16 september 2017](https://nllgg.nl/bijeenkomst/20170916) hebben we geëxperimenteerd met het bouwen van een Multiseat computer. Het doel was om een PC uit te rusten met meerdere VGA kaarten, toetsenborden en muizen en daar dan met meerdere mensen tegelijk op te kunnen werken. Het is gelukt, ging gemakkelijker dan gedacht en hieronder schrijven we wat we gedaan hebben.

## Geschiedenis

In 2014 heb ik dit al eens geprobeerd en dat wilde toen niet erg vlotten. Ik probeerde het toen met 3 Nvidia VGA kaarten in een oude PC. Daarop had ik Debian Wheezy en de NVidia driver geïnstalleerd. Dat werkte niet goed met de kernel destijds. Een bug (kan 'm nu natuurlijk niet meer vinden) in de Nvidia driver werkte niet kernel. Na een tijdje proberen had ik er geen tijd meer voor. Ik probeerde het toen in de Xorg config te regelen met GDM3 die, zo ontdekte ik na wat zoeken, helemaal geen multiseat meer ondersteunt. Nog even met LightDM geprobeerd en dat wilde ook niet lukken. Kijk maar in de [commit historie](https://gitlab.com/nllgg/multiseat-linux/commits/master) om een idee te krijgen van al mijn pogingen.

## Hardware

De PC had ik nog staan en nog steeds de wens om voor mijn kinderen drie werkplekken te maken met één PC. Het leek me leuk om het als NLLGG activiteit te proberen omdat daar altijd veel kennis samenkomt.
De PC bestaat uit:
- Een Coolermaster kast.
- Een [Gigabyte GA-770TA-UD3](https://www.gigabyte.com/Motherboard/GA-770TA-UD3-rev-10) moederbord.
- Een AMD Phenom II X4 955 processor.
- 16G geheugen.
- 2 SSD's in (MDraid) mirror (ik gebruik nooit de RAID controller van het moederbord).
- 3 Nvidia GeForce GT610 kaarten:
  - 1x [Gigabyte GeForce GT 610 1GB LP (GV-N610D3-1GI)](http://www.gigabyte.co.nl/Graphics-Card/GV-N610D3-1GI) (in het PCI express x16).
  - 2x [Zotac GeForce GT 610 PCIe x1 (ZT-60605-10L)](https://www.zotac.com/nl/product/graphics_card/geforce-%C2%AE-gt-610-pcie-x1) (in de PCI express x1 sloten).
  Op deze manier hoopte het voordeel te hebben met één driver toe te kunnen maar uiteindelijk maakt dat niet zoveel uit volgens mij.
- 3 sets van schermen/muizen/toetsenborden:
  - Seat0:
    - Scherm: Acer 1920x1080.
    - Keyboard: Compaq PS/2 op USB naar PS/2 adapter.
    - Muis: Logitech USB muis.
  - Seat-1:
    - Eén Samsung 1680x1050.
    - Keyboard: Compaq PS/2 op PS/2 poort.
    - Muis: Logitech USB muis.
  - Seat-2:
    - Eén (merk vergeten) 1280x1024.
    - Keyboard & Muis: Logitech USB wireless desktop.

## Software
Op deze computer was, op het moment van het experiment, Debian Stretch met de 4.9 kernel uit backports geïnstalleerd. Als Displaymanager LightDM. Verder verschillende window managers om zo iedereen die mee deed een plezier te doen. Ik had ook nog niet de NVidia driver geïnstalleerd. Bleek ook helemaal niet nodig, de Nouveau driver deed het zonder problemen.

## Het experiment
Tijdens het experiment vonden we al snel dat er sinds 2014 waardevolle documentatie bijgekomen is: De [Archlinux documentatie](https://wiki.archlinux.org/index.php/xorg_multiseat) was het beste. Eén aanpassing deden we aan de LightDM config file `/etc/lightdm/lightdm.conf`:
```
[Seat:seat0]
xserver-command=/usr/lib/xorg/Xorg :0
xserver-layout=seat0

[Seat:seat-1]
xserver-command=/usr/lib/xorg/Xorg :1
xserver-layout=seat-1

[Seat:seat-2]
xserver-command=/usr/lib/xorg/Xorg :2
xserver-layout=seat-2
```
> Merk de namen van de seats op: LightDM vindt het echt belangrijk dat `seat0` niet `seat-0` wordt. Dat werkt niet.

Daarna was het eigenlijk alleen maar puzzelwerk met `lspci` en `loginctl`. In de documentatie staan een paar handige tips over hoe hardware te identificeren zoals `cat /dev/input/mouse1` om uit te vinden welke fysieke muis bij welke logisch device hoort. Zelfde kan met keyboards, die heten `event#` in `/dev/input`.

## De praktijk
Onlangs had ik eindelijk de gelegenheid om de computer thuis in gebruik te nemen. Omdat ik niet alle schermen/muizen/keyboards op dezelfde manier als tijdens het experiment had aangesloten moest ik opnieuw alles identificeren. Hartstikke lachen met 3 ongeduldige kinderen om je heen. (Ze 'helpen' dan mee door allemaal de muis te bewegen als je dat aan één kind hebt gevraagd).
Uiteindelijk werkte alles:
![Het testpanel](images/multiseat_werkt.jpg)

De uitendelijke configuratie op mijn PC:
```
# seat0
loginctl attach seat0 /sys/devices/pci0000:00/0000:00:02.0/0000:01:00.0/drm/card0
loginctl attach seat0 /sys/devices/pci0000:00/0000:00:12.0/usb5/5-2/5-2:1.0/0003:10D5:0004.0001/input/input3
loginctl attach seat0 /sys/devices/pci0000:00/0000:00:13.1/usb8/8-3/8-3:1.0/0003:046D:C016.0006/input/input8

# seat-1
loginctl attach seat-1 /sys/devices/pci0000:00/0000:00:04.0/0000:02:00.0/drm/card1
loginctl attach seat-1 /sys/devices/platform/i8042/serio0/input/input0
loginctl attach seat-1 /sys/devices/pci0000:00/0000:00:12.1/usb6/6-3/6-3:1.0/0003:046D:C040.0005/input/input7

# seat-2
loginctl attach seat-2 /sys/devices/pci0000:00/0000:00:05.0/0000:03:00.0/drm/card2
loginctl attach seat-2 /sys/devices/pci0000:00/0000:00:13.1/usb8/8-2/8-2:1.0/0003:046D:C517.0007/input/input24
loginctl attach seat-2 /sys/devices/pci0000:00/0000:00:13.1/usb8/8-2/8-2:1.1/0003:046D:C517.0008/input/input25
```
